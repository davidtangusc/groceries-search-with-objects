var groceriesData = [
  'Organic Mixed Greens',
  'Dark Chocolate Almonds',
  'Almond Milk',
  'Peanut Butter',
  'Peaches',
  'Water',
  'Green Machine',
  'Beer'
];


var groceriesList = {
  initialize: function(options) {
    this.groceries = options.data;
    this.$element = $(options.element);
  },

  _createGroceryHtml: function(grocery) {
    return '<li>'+ grocery +'</li>';
  },

  render: function() {
    var html = '';
    var li;

    for (var i = 0; i < this.groceries.length; i++) {
      li = this._createGroceryHtml(this.groceries[i]);
      html = html + li;
    }

    this.$element.html(html).hide().fadeIn(300);
  },

  filterBy: function(searchTerm) {
    var results = '';
    var groceryItem;

    searchTerm = searchTerm.toLowerCase();

    for (var i = 0; i < this.groceries.length; i++) {
      groceryItem = this.groceries[i].toLowerCase();

      if (groceryItem.indexOf(searchTerm) > -1) {
        // searchTerm was found in current grocery item
        results = results + this._createGroceryHtml(this.groceries[i]);
      }
    }

    this.$element.html(results);
  }
};

// Start up the app
groceriesList.initialize({
  data: groceriesData,
  element: '#groceries'
});

groceriesList.render();

$('input#search').on('keyup', function() {
  var searchTerm = this.value;
  groceriesList.filterBy(searchTerm);
});









